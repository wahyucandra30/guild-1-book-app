const fs = require("fs");

// reading data books
const loadBooks = () => {
	const fileBuffer = fs.readFileSync("data/books.json", "utf-8");
	const books = JSON.parse(fileBuffer);
	return books;
};
module.exports = {
	loadBooks
};