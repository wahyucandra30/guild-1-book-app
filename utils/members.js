const fs = require("fs");

// reading data members
const loadMembers = () => {
	const fileBuffer = fs.readFileSync("data/members.json", "utf-8");
	const members = JSON.parse(fileBuffer);
	return members;
};
module.exports = {
	loadMembers
};