const fs = require("fs");

// reading data kategori
const loadKategori = () => {
	const fileBuffer = fs.readFileSync("data/kategori.json", "utf-8");
	const kategori = JSON.parse(fileBuffer);
	return kategori;
};
module.exports = {
	loadKategori
};