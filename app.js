const express = require("express");
const app = express();
const {loadMembers} = require("./utils/members");
const {loadKategori} = require("./utils/kategori");
const {loadBooks} = require("./utils/books");

const port = 8080;

app.set("view engine", "ejs");
app.use("/public", express.static("./public"));


app.get("/", (req, res) =>{
	const members = loadMembers();
	const books = loadBooks();
	res.status(200);
	res.render("index", {title: "Home", members, books});
});

app.get("/kategori", (req, res) => {
	const kategori = loadKategori();
	res.status(200);
	res.render("kategori", {title: "Kategori", kategori});
});

app.get("/detailbook", (req, res) => {
	res.status(200);
	res.render("detailBook", {title: "Detail Book"});
});

app.get("/login", (req, res) => {
	res.status(200);
	res.render("login", {title: "Login"});
});

app.get("/register", (req, res) => {
	res.status(200);
	res.render("register", {title: "Register"});
});

app.get("/shoppingcart", (req, res) => {
	res.status(200);
	res.render("shoppingCart", {title: "Shopping Cart"});
});



app.listen(port, () => console.log(`Server running on port ${port}`));